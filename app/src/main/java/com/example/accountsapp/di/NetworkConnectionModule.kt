package com.example.accountsapp.di

import com.example.accountsapp.network.AccountService
import com.example.accountsapp.utils.AccountConstants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkConnectionModule {


    @Singleton
    @Provides
    fun httpClientModule(): OkHttpClient = OkHttpClient.Builder().readTimeout(15, TimeUnit.SECONDS)
        .connectTimeout(15, TimeUnit.SECONDS).build()

    @Singleton
    @Provides
    fun gsonConvertorObj(): GsonConverterFactory = GsonConverterFactory.create()


    @Singleton
    @Provides
    fun providerRetroFit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL).client(okHttpClient).addConverterFactory(gsonConverterFactory).build()

    @Singleton
    @Provides
    fun providenetworkObj(retrofit: Retrofit):AccountService =retrofit.create(AccountService::class.java)

}