package com.example.accountsapp.repositories

import com.example.accountsapp.network.AccountService
import javax.inject.Inject

class AccountsRepository @Inject constructor(private val accountService: AccountService) {
    fun getAllData() = accountService.getAllData()
}