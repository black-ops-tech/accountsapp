package com.example.accountsapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.accountsapp.R
import com.example.accountsapp.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    lateinit var _bind: ActivityDetailBinding
//    var id: Int = 0
//    lateinit var name: String
//    var amount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _bind = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(_bind.root)
    }

//    private fun getDataForDetail() {
//        id = intent.getIntExtra("ID", 0)
//        name = intent.getStringExtra("NAME").toString()
//        amount = intent.getIntExtra("AMT", 0)
//    }
}