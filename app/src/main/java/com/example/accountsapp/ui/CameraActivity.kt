package com.example.accountsapp.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.FileUtils
import android.provider.MediaStore
import android.util.Log
import com.example.accountsapp.databinding.ActivityCameraBinding
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class CameraActivity : AppCompatActivity() {

    lateinit var bind: ActivityCameraBinding
    val REQUEST_IMAGE_CAPTURE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = ActivityCameraBinding.inflate(layoutInflater)
        setContentView(bind.root)
        bind.btnCamera.setOnClickListener {
            dispatchTakePictureIntent()
        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            Log.d("ERROR:", e.message.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            var document: PdfDocument = PdfDocument()
            var pdfFile: File =
                File(
                    this.externalCacheDir?.absolutePath
                            + File.separator
                            + "TemproraryPDF_"
                            + System.currentTimeMillis()
                            + ".pdf"
                )
            try {
                val imageBitmap = data?.extras?.get("data") as Bitmap

                var pageinfo =
                    PdfDocument.PageInfo.Builder(imageBitmap.width, imageBitmap.height, 1).create()
                var page = document.startPage(pageinfo)
                var canvas: Canvas = page.canvas
                var paint: Paint = Paint()
                paint.setColor(Color.parseColor("#ffffff"))
                canvas.drawPaint(paint)
//                canvas.drawBitmap(imageBitmap, 0, 0, null)
                document.finishPage(page)
                bind.imgViewer.setImageBitmap(imageBitmap)
                document.writeTo(FileOutputStream(pdfFile))

                if (pdfFile.exists() && pdfFile.length() > 0) {
                    println("RES:: " + pdfFile.absolutePath)
                    bind.pdfPath.text = pdfFile.absolutePath.toString()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                document.close()
            }

        }
    }
}