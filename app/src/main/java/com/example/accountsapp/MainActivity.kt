package com.example.accountsapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.accountsapp.databinding.ActivityMainBinding
import com.example.accountsapp.ui.CameraActivity
import com.example.accountsapp.viewmodels.AccountsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private val accountsViewModel by viewModels<AccountsViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fetchData()
        setGridView()
        binding.jumpToSec.setOnClickListener {
            startActivity(Intent(this, CameraActivity::class.java))
        }
    }

    private fun setGridView() {
        binding.tableView.let {

        }
    }

    private fun fetchData() {
        var Response = accountsViewModel.fetchData()
        println("RES:: ${Response}")
    }
}