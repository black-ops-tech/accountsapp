package com.example.accountsapp.model

data class AccountsData(val actId: Int, val actName: String, val amount: Int)
