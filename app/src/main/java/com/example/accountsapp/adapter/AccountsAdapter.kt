package com.example.accountsapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.accountsapp.R
import com.example.accountsapp.ui.DetailActivity

class AccountsAdapter(context: Context, list: List<String>) : BaseAdapter() {
    private var layoutInflater: LayoutInflater? = null
    private lateinit var actId: TextView
    private lateinit var actName: TextView
    private lateinit var actAmount: TextView
    private lateinit var deleteItem: ImageView
    private lateinit var rowitemlayoutview: LinearLayout
    var list = list
    var ctx = context
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(p0: Int): Any? {
        return null
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        var convertView = p1
        if (layoutInflater == null) {
            layoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        if (convertView == null) {
            convertView = layoutInflater!!.inflate(R.layout.rowitem_layout, null)
        }

        actId = convertView!!.findViewById(R.id.actId)
        actName = convertView!!.findViewById(R.id.actName)
        actAmount = convertView!!.findViewById(R.id.actAmount)
        deleteItem = convertView!!.findViewById(R.id.delete_item)
        rowitemlayoutview = convertView!!.findViewById(R.id.layout_on_rowitem)



//        rowitemlayoutview.setOnClickListener {
//        val intent = Intent(ctx, DetailActivity::class.java)
//        intent.putExtra("ID", list.get(p0).id)
//        intent.putExtra("NAME", list.get(p0).name)
//        intent.putExtra("AMT", list.get(p0).amount)
//            ctx.startActivity(intetn)
//        }

        //make modification of mapping parameters below....
        actId.text = list.get(p0)
        actName.text = list.get(p0)
        actAmount.text = list.get(p0)

        //Deletion code here...

        return convertView
    }


}