package com.example.accountsapp.network

import retrofit2.Call
import retrofit2.http.GET

interface AccountService {

    @GET("payments/owner")
    fun getAllData(): Call<String>
}