package com.example.accountsapp.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.example.accountsapp.repositories.AccountsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@HiltViewModel
class AccountsViewModel @Inject constructor(
    private val accountsRepository: AccountsRepository,
    application: Application
) : AndroidViewModel(application) {

    fun fetchData() {
        accountsRepository.getAllData().enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                println("RES:: ${response.body()}")
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("Error:", "${t.printStackTrace()}")
            }
        })
    }

    fun createPDF(context: Context, data:ArrayList<File>){

    }
}